import os
import h5py
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

sns.set_context('paper', font_scale=1.5)
sns.set_style('white')

def network_prediction():
    from keras.models import load_model
    os.environ['CUDA_VISIBLE_DEVICES'] = '1'
    ## load data
    nbins = 56
    data_path = os.path.join("../preprocessing/v2_vs_dndy_56x56_train_valid_test.h5")
    h5file = h5py.File(data_path, "r")
    x_train = h5file["x_train"][...]
    y_train = h5file["y_train"][...]
    x_validation = h5file["x_validation"][...]
    y_validation = h5file["y_validation"][...]
    x_test = h5file["x_test"][...]
    y_test = h5file["y_test"][...]
    model_noabs = load_model("ckpt/squeezenet_regression_v2_vs_dndy_noabs.hdf5")
    #model_withabs = load_model("ckpt/squeezenet_regression_v2_vs_dndy_absbeta2.hdf5")
    model_withabs = load_model("ckpt/squeezenet_regression_v2_vs_dndy_absbeta2_noabsbeta4.hdf5")

    noabs_pred = model_noabs.predict(x_test)
    wiabs_pred = model_withabs.predict(x_test)
    df = pd.DataFrame({'true_beta2':y_test[:, 0],
                       'true_beta4':y_test[:, 1],
                       'pred1_beta2':noabs_pred[:, 0],
                       'pred1_beta4':noabs_pred[:, 1],
                       'pred2_absbeta2':wiabs_pred[:, 0],
                       'pred2_beta4':wiabs_pred[:, 1],
                      })
    df.to_csv("prediction_data_nobeta4abs.csv")


def make_plot(data, saveas="figs/regression_res.png", ms=1):
    plt.figure(figsize=(12, 3))
    plt.subplot(1, 4, 1)
    plt.plot(data['true_beta2'], data['pred1_beta2'], 'bo', markersize=ms)
    plt.text(0.2, 0.8, r'$\beta_2$', transform=plt.gca().transAxes)
    plt.xlabel('ground truth')
    plt.ylabel('network prediction')
    
    plt.subplot(1, 4, 2)
    plt.plot(data['true_beta4'], data['pred1_beta4'], 'bo', markersize=ms)
    plt.text(0.2, 0.8, r'$\beta_4$', transform=plt.gca().transAxes)
    plt.xlabel('ground truth')
    plt.ylabel('network prediction')
    
    plt.subplot(1, 4, 3)
    plt.plot(data['true_beta2'].abs(), data['pred2_absbeta2'], 'bo', markersize=ms)
    plt.text(0.2, 0.8, r'$|\beta_2|$', transform=plt.gca().transAxes)
    plt.xlabel('ground truth')
    plt.ylabel('network prediction')
    
    plt.subplot(1, 4, 4)
    plt.plot(data['true_beta4'], data['pred2_beta4'], 'bo', markersize=ms)
    plt.text(0.2, 0.8, r'$|\beta_4|$', transform=plt.gca().transAxes)
    plt.xlabel('ground truth')
    plt.ylabel('network prediction')
    
    plt.tight_layout()
    plt.savefig(saveas)


if __name__ == '__main__':
    network_prediction()
    data = pd.read_csv("prediction_data_nobeta4abs.csv")
    make_plot(data)
