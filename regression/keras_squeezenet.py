import os
import h5py
import keras
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Dense, Conv2D, MaxPool2D, BatchNormalization, Activation, GlobalAveragePooling2D
from keras import backend as K

os.environ['CUDA_VISIBLE_DEVICES'] = '2'

# load the data
nbins = 56
#data_path = os.path.join("../nuclear_structure/", "entropy_vs_ecc2_%sx%s_train_valid_test.h5"%(nbins,nbins))
data_path = os.path.join("../data/", "../preprocessing/v2_vs_dndy_56x56_train_valid_test.h5")
h5file = h5py.File(data_path, "r")
x_train = h5file["x_train"][...]
y_train = h5file["y_train"][...]
x_validation = h5file["x_validation"][...]
y_validation = h5file["y_validation"][...]
x_test = h5file["x_test"][...]
y_test = h5file["y_test"][...]
 
def Residual(x, num_channels, use_1x1conv=False, strides=1, **kwargs):
    conv1 = Conv2D(num_channels, kernel_size=3, padding='same', strides=strides)(x)
    batch1 = BatchNormalization()(conv1)
    Y = Activation('relu')(batch1)
    conv2 = Conv2D(num_channels, kernel_size=3, padding='same')(Y)
    Y = BatchNormalization()(conv2)
    Y = SqueezeExcitation(Y)
    if use_1x1conv:
        x = Conv2D(num_channels, kernel_size=1, strides=strides)(x)

    return Activation('relu')(keras.layers.add([x, Y]))

def SqueezeExcitation(x, **kwargs):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    Y = GlobalAveragePooling2D()(x)
    input_shape = K.int_shape(x)
    channels = input_shape[channel_axis]
    Y = Dense(channels // 16, activation='relu', kernel_initializer='he_normal', use_bias=False)(Y)
    Y = Dense(channels, activation='sigmoid', kernel_initializer='he_normal', use_bias=False)(Y)
    Y = keras.layers.Reshape((1, 1, channels))(Y)
    if K.image_data_format() == 'channels_first':
        Y = Permute((3, 1, 2))(Y)
    Y = keras.layers.multiply([x, Y])
    #print(K.int_shape(se))
    return Y


def resnet_block(x, num_channels, num_residuals, first_block=False):
    for i in range(num_residuals):
        if i == 0 and not first_block:
            x = Residual(x, num_channels, use_1x1conv=True, strides=2)
        else:
            x = Residual(x, num_channels)
    return x

num_classes = 2
input_img = keras.models.Input(shape=(nbins, nbins, 1))
X = Conv2D(64, kernel_size=3, strides=1, padding='same')(input_img)
X = BatchNormalization()(X)
X = Activation('relu')(X)
X = resnet_block(X, 64, 3, first_block=True)
X = resnet_block(X, 128, 4)
X = resnet_block(X, 256, 6)
X = resnet_block(X, 512, 3)
X = GlobalAveragePooling2D()(X)
X = Dense(num_classes)(X)

res34 = keras.models.Model(input_img, X)

res34.compile(loss='mse',
              #optimizer=keras.optimizers.SGD(lr=0.1, momentum=0.9, decay=1.0E-5),
              optimizer=keras.optimizers.Adam(),
              metrics=['acc'])

callbacks_list = [keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=10), 
    keras.callbacks.ModelCheckpoint(filepath="ckpt/squeezenet_regression_v2_vs_dndy_absbeta2_noabsbeta4.hdf5", verbose=1, save_best_only=True)]

# train the residual network

#res34.fit(x_train, y_train, epochs=20, verbose=1,
#                         callbacks=callbacks_list,
#                         validation_data=(x_validation, y_validation))
#
#print(res34.evaluate(x_test, y_test))

#res34.fit(x_train, np.abs(y_train), epochs=20, verbose=1,
#                         callbacks=callbacks_list,
#                         validation_data=(x_validation, np.abs(y_validation)))
#
#print(res34.evaluate(x_test, np.abs(y_test)))

y_train[:, 0] = np.abs(y_train[:, 0])
y_validation[:, 0] = np.abs(y_validation[:, 0])
y_test[:, 0] = np.abs(y_test[:, 0])

res34.fit(x_train, y_train, epochs=20, verbose=1,
                 callbacks=callbacks_list,
                 validation_data=(x_validation, y_validation))

print(res34.evaluate(x_test, y_test))
