#Email: lgpang@qq.com

from keras.preprocessing import image
from keras import backend as K
import numpy as np
import matplotlib.pyplot as plt
import cv2

def load_img(img_path, target_size=(224, 224), preprocess_func=None):
    '''load image and preprocess it
    Return:
        numpy array of shape (1, height, width, 3) or (1, height, width, 1)
    '''
    img = image.load_img(img_path, target_size=target_size)
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    if preprocess_func:
        x = preprocess_func(x)
    return x



class CAM(object):
    def __init__(self, model, last_conv_name, img, grad_cam = True,
            task_type='classification', regression_target=0):
        '''Class Activation Map (CAM) implementation
        learned from book [Deep learning with Python], with one bug fixed for 
        gradient weighted cam, where ReLU has to be applied to the weighted activation map.
        Native cam is added to the implementation.
        Cam and grad-cam for regression task are also included in the current wrapper.

        Args:
               model: classifier or regressor from Keras
               last_conv_name: the name of the last convolution layer
               img: reshaped into size (1, height, width, channels) where channels = 1 or 3,
               grad_cam: False for native cam and True for gradient weighted cam
               task_type: one of ['classification', 'regression']
               regression_target: e.g., if task_type=='regression', the final layer is Dense(N),
                   then regression_target is the id of one the output neuron in [0, 1, ..., N-1].
        Examples:
            from cam import CAM
            from keras.applications.resnet50 import preprocess_input, decode_predictions
            from keras.applications.resnet50 import ResNet50

            grad_cam = True
            img_path = 'imgs/elephant.jpg'
            img = load_img(img_path, preprocess_func=preprocess_input)

            model = ResNet50(weights='imagenet')
            last_conv_name = 'activation_49'
            cam = CAM(model, last_conv_name, img, grad_cam)
            cam.plot_heatmap()
            saveto = "resnet50_elephant_cam.jpg"
            if grad_cam:
                saveto = "resnet50_elephant_gradcam.jpg"
            cam.plot_imposed_img(saveto)
            cam.predictions(decode_predictions)
            cam.model.summary()
        '''
        self.model = model

        x = img

        preds = model.predict(x)

        self.preds = preds

        if task_type == 'classification':
            class_id = np.argmax(preds[0])
            #print("class_id = ", class_id)
        elif task_type == 'regression':
            class_id = regression_target
            #print("regression target = ", class_id)
        else:
            print("task_type must be 'classification' or 'regression'")

        output = model.output[:, class_id]
        # The is the output feature map of the designated `conv` layer,
        # the last convolutional layer in the model before global_avg_pooling
        # in practice, one can experiment with any convolution layer, especially 
        # if the size of the last conv layer is too small
        last_conv_layer = model.get_layer(last_conv_name)
        
        # This is the gradient of the output class with regard to
        # the output feature map of the last conv layer
        grads = K.gradients(output, last_conv_layer.output)[0]
        
        # This is a vector of shape (channels,), where each entry
        # is the mean intensity of the gradient over a specific feature map channel
        pooled_grads = K.mean(grads, axis=(0, 1, 2))
        
        # This function allows us to access the values of the quantities we just defined:
        # `pooled_grads` and the output feature map given a sample image
        iterate = K.function([model.input], [pooled_grads, last_conv_layer.output[0]])
        
        # These are the values of these two quantities, as Numpy arrays, given our sample image 

        pooled_grads_value, conv_layer_output_value = iterate([x])

        # We multiply each channel in the feature map array
        # by "how important this channel is" with regard to the elephant class
        channels = conv_layer_output_value.shape[-1]
        #print("num of channels = ", channels)
        if grad_cam:
            for i in range(channels):
                conv_layer_output_value[:, :, i] *= pooled_grads_value[i]
            # add Relu activation to select positive contributions only
            # notice that without Relu or np.abs(), the gradcam result is wrong when
            # several predicted classes have similar probabilities. E.g., activation maps
            # for tusker and Indian_elephant will cancel each other in the important regions for both classes
            if task_type != 'regression':
                conv_layer_output_value = np.maximum(conv_layer_output_value,\
                                         np.zeros_like(conv_layer_output_value))

        # The channel-wise mean of the resulting feature map
        # is our heatmap of class activation
        heatmap = np.mean(conv_layer_output_value, axis=-1)
        heatmap = np.maximum(heatmap, 0)
        heatmap /= np.max(heatmap)
        self.heatmap = heatmap


    def plot_heatmap(self):
        plt.matshow(self.heatmap)
        plt.show()

    def plot_imposed_img(self, img_path, save_as="class_activation_map.png"):
        # We use cv2 to load the original image
        img = cv2.imread(img_path)
        # We resize the heatmap to have the same size as the original image
        heatmap = cv2.resize(self.heatmap, (img.shape[1], img.shape[0]))
        # We convert the heatmap to RGB
        heatmap = np.uint8(255 * heatmap)
        heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_JET)
        # 0.4 here is a heatmap intensity factor
        superimposed_img = heatmap * 0.4 + img
        # Save the image to disk
        if save_as: 
            cv2.imwrite(save_as, superimposed_img)
            #print("Save class activation map to %s"%save_as)

    def predictions(self, decode_predictions=None):
        if decode_predictions:
            print('Predicted:', decode_predictions(self.preds, top=3)[0])
        else:
            print('Predicted class idx = ',  np.argmax(self.preds[0]))



def vgg16_elephant_cam(grad_cam=True):
    img_path = 'imgs/elephant.jpg'
    from keras.applications.vgg16 import preprocess_input, decode_predictions
    from keras.applications.vgg16 import VGG16
    model = VGG16(weights='imagenet')
    last_conv_name = 'block5_conv3'
    img = load_img(img_path, preprocess_func=preprocess_input)
    cam = CAM(model, last_conv_name, img, grad_cam)
    cam.plot_heatmap()
    saveto = "imgs/vgg16_elephant_cam.jpg"
    if grad_cam:
        saveto = "imgs/vgg16_elephant_gradcam.jpg"
    cam.plot_imposed_img(img_path, saveto)
    cam.predictions(decode_predictions)

def vgg19_elephant_cam(grad_cam=True):
    ### vgg19 has 3 fully connect layers, but the native cam still works
    ### the network structure is not limited to must have a global averge pooling layer
    ### before final layer
    img_path = 'imgs/elephant.jpg'
    from keras.applications.vgg19 import preprocess_input, decode_predictions
    from keras.applications.vgg19 import VGG19
    model = VGG19(weights='imagenet')
    model.summary()
    last_conv_name = 'block5_conv4'
    img = load_img(img_path, preprocess_func=preprocess_input)
    cam = CAM(model, last_conv_name, img, grad_cam)
    cam.plot_heatmap()
    saveto = "imgs/vgg19_elephant_cam.jpg"
    if grad_cam:
        saveto = "imgs/vgg19_elephant_gradcam.jpg"
    cam.plot_imposed_img(img_path, saveto)
    cam.predictions(decode_predictions)

def inceptionv3_elephant_cam(grad_cam=True):
    img_path = 'imgs/elephant.jpg'
    from keras.applications.inception_v3 import preprocess_input, decode_predictions
    from keras.applications.inception_v3 import InceptionV3
    model = InceptionV3(weights='imagenet')
    last_conv_name = 'conv2d_94'
    img = load_img(img_path, preprocess_func=preprocess_input)
    cam = CAM(model, last_conv_name, img, grad_cam)
    cam.plot_heatmap()
    saveto = "imgs/inceptionv3_elephant_cam.jpg"
    if grad_cam:
        saveto = "imgs/inceptionv3_elephant_gradcam.jpg"
    cam.plot_imposed_img(img_path, saveto)
    cam.predictions(decode_predictions)
    #cam.model.summary()

def res50_elephant_cam(grad_cam=True):
    img_path = 'imgs/elephant.jpg'
    from keras.applications.resnet50 import preprocess_input, decode_predictions
    from keras.applications.resnet50 import ResNet50
    model = ResNet50(weights='imagenet')
    last_conv_name = 'activation_49'
    img = load_img(img_path, preprocess_func=preprocess_input)
    cam = CAM(model, last_conv_name, img, grad_cam)
    cam.plot_heatmap()
    saveto = "imgs/resnet50_elephant_cam.jpg"
    if grad_cam:
        saveto = "imgs/resnet50_elephant_gradcam.jpg"
    cam.plot_imposed_img(img_path, saveto)
    cam.predictions(decode_predictions)
    cam.model.summary()


if __name__ == '__main__':
    import os
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    vgg16_elephant_cam(grad_cam=True)
    vgg19_elephant_cam(grad_cam=True)
    inceptionv3_elephant_cam(grad_cam=True)
    res50_elephant_cam(grad_cam=False)
