from cam import CAM, load_img
import h5py
import numpy as np
import os
import cv2
import matplotlib.pyplot as plt
import seaborn as sns
from keras.models import load_model

sns.set_context('paper', font_scale=1.5)
sns.set_style('white')


## load data
nbins = 56
data_path = os.path.join("../preprocessing/v2_vs_dndy_56x56_train_valid_test.h5")
h5file = h5py.File(data_path, "r")
x_train = h5file["x_train"][...]
y_train = h5file["y_train"][...]
x_validation = h5file["x_validation"][...]
y_validation = h5file["y_validation"][...]
x_test = h5file["x_test"][...]
y_test = h5file["y_test"][...]

def cam_for_event(idx=0, model=None, last_conv='conv2d_7'):
    img = np.expand_dims(x_test[idx], axis=0)
    grad_cam = True
    cam = CAM(model, last_conv, img, grad_cam,\
              task_type="regression", regression_target=0)
    #cam.plot_heatmap()
    heatmap = cam.heatmap
    heatmap = cv2.resize(heatmap, (img.shape[2], img.shape[1]))
    superimposed_img = heatmap * img[0, :, :, 0]
    return img, heatmap, superimposed_img


def plot_cam_for_layers_horizental(eids=[2, 0, 3, 1], 
                   model = None,
                   last_conv_list=['add_1', 'add_2'],
                   cmap = 'viridis',
                   save_as="imgs/beta2_grad_cam_layers_horizental.png"):
    n = len(eids)
    m = len(last_conv_list)
    plt.figure(figsize=(3*(m+1), 3*n))

    for i in range(n):
        for j in range(m+1):
            layer_name = last_conv_list[j-1] if j > 0 else 'input image'
            print(layer_name, " finished!")
            if j == 0:
                img = np.expand_dims(x_test[eids[i]], axis=0)
            else:
                img, heatmap, superimposed_img = cam_for_event(eids[i], model=model,
                                                           last_conv=layer_name)  
            plt.subplot(n, m+1, j+i*(m+1)+1)
            if j == 0:
                plt.imshow(img[0,:,:,0].T, origin='lower', cmap=cmap)
                plt.axis('off') 
                plt.text(-0.15, 0.8, r'$|\beta_2|=%s$'%np.abs(y_test[eids[i]][0]), 
                              color='k', 
                              fontsize=25,
                              rotation=90,
                        transform = plt.gca().transAxes)

            else:
                plt.imshow(heatmap.T, origin='lower', cmap=cmap)
                plt.axis('off')
            
            if i == 0: 
                    plt.title(layer_name, color='k', size=25)
             
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.02, wspace=0.02)
    plt.savefig(save_as)

if __name__ == '__main__':
    import os
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    squeeze_net = load_model("../regression/ckpt/squeezenet_regression_v2_vs_dndy_noabs.hdf5")
    plot_cam_for_layers_horizental(model=squeeze_net,    
                save_as='imgs/beta2_grad_cam_layers_for_squeezenet_horizental.png',
                last_conv_list=['add_1', 'add_2', 'add_4', 'add_8', 'add_12','add_16'])

