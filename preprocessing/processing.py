import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
import seaborn as sns
from tqdm import tqdm
import h5py

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split

from matching import elliptic_flow, scale_entropy

sns.set_context('poster', font_scale=0.8)
sns.set_style('ticks')

class DeformedNucleiData(object):
    '''Use 80% for training and 20% for testing when test_size=0.2.
    For each training data, sample boot_strap_size points which is 90% of 
    boot_strap_from_size for daga augmentation in PointNet'''
    def __init__(self, batch_size=32, validation_size=0.1, test_size=0.1,
          boot_strap_from_size=50000, 
          use_columns=['s0', 'e2'],
          path = "../data"):
        self.use_columns = use_columns    
        self.ntotal = 2601
        indices = np.arange(self.ntotal)
        np.random.shuffle(indices)
        ntrain = int(self.ntotal*(1-validation_size-test_size))
        nvalid = int(self.ntotal*validation_size)
        ntest = int(self.ntotal*test_size)
        self.train_id = indices[:ntrain]
        self.validation_id = indices[ntrain:ntrain+nvalid]
        self.test_id = indices[ntrain+nvalid:]
        self.target = np.loadtxt(os.path.join(path, 'params.csv'))
        self.path = path
        self.boot_strap_from_size = boot_strap_from_size
        self.boot_strap_size = int(0.9*boot_strap_from_size)
        self.batch_size = batch_size
        
    def read_sample(self, idx):
        if idx < 0 or idx >self.ntotal:
            print("idx must be in range [0, 2600]")
        header = np.array(['b', 'npart', 's0', 'e2', 'e3',
                           'e4', 'e5', 'psi2', 'psi3', 'psi4', 'psi5'])
        uu = pd.read_csv(os.path.join(path, 'UU%s.csv'%idx), 
                     sep='\s+', header=None,
                     usecols=range(1, 12), dtype=np.float32)
        uu.columns = header
        uu = uu.sort_values(by=['s0'], ascending=False).loc[:, self.use_columns]
        return uu.iloc[:self.boot_strap_from_size].values

    def __boot_strap(self, idx):
        '''Sample self.boot_strap_size num of events out of all
        the 100000 points in the sample'''
        dat = self.read_sample(idx)
        nitems = len(dat)
        ids = np.random.choice(nitems, self.boot_strap_size, replace=False)
        return dat[ids]
        
    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' 
        # X : (n_samples, *dim, n_channels)
        # Initialization
        num_of_channels = len(self.use_columns)
        X = np.empty((self.batch_size, self.boot_strap_size, num_of_channels))
        y = np.empty((self.batch_size, 2), dtype=np.float32)
        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            X[i,] = self.__boot_strap(ID)
            # Scale the feature values
            #scaler = StandardScaler()
            #X[i,] = scaler.fit_transform(X[i,])
            # Store class
            y[i] = self.target[ID]

        return X, y
            
    def training_batch(self):
        list_ids = np.random.choice(self.train_id, self.batch_size)
        x_train, y_train = self.__data_generation(list_ids)
        return x_train, y_train
    
    def validation_batch(self):
        list_ids = np.random.choice(self.validation_id, self.batch_size)
        x_valid, y_valid = self.__data_generation(list_ids)
        return x_valid, y_valid
    
    def testing_batch(self):
        list_ids = np.random.choice(self.test_id, self.batch_size)
        x_test, y_test = self.__data_generation(list_ids)
        return x_test, y_test

def create_images(nbatchs=2, kind='training', nbins=56, use_columns=['s0', 'e2'], path='../data'):
    '''create histogram images for regression, ecc2 is matched to v2 and
       initial total entropy is self-normalized for each nuclei configuration'''
    deform = DeformedNucleiData(use_columns=use_columns, path=path)
    x_, y_ = None, None
    x_imgs = []
    y_labs = []
    for b in tqdm(range(nbatchs)):
        if kind == 'training':
            x_, y_ = deform.training_batch()
        elif kind == 'validation':
            x_, y_ = deform.validation_batch()
        elif kind == 'testing':
            x_, y_ = deform.testing_batch()
        for i in range(len(x_)):
            imgs = []
            s0 = x_[i, :, 0]
            ecc2 = x_[i, :, 1]
            v2 = elliptic_flow(ecc2)
            dndy = scale_entropy(s0, top_percent=0.02)
            img, xe, ye = np.histogram2d(dndy, v2, bins=nbins, density=True)
            imgs.append(img)
            x_imgs.append(np.dstack(imgs))
            y_labs.append(y_[i])
    return np.array(x_imgs), np.array(y_labs)



if __name__ == '__main__':
    path = "../data"
    nbins = 56
    use_columns = ['s0', 'e2']
    x_train, y_train = create_images(nbatchs=5000, kind='training')
    x_validation, y_validation = create_images(nbatchs=500, kind='validation')
    x_test, y_test = create_images(nbatchs=500, kind='testing')
    with h5py.File("v2_vs_dndy_%sx%s_train_valid_test.h5"%(nbins,nbins), "w") as h5file:
        h5file.create_dataset("x_train", data=np.array(x_train))
        h5file.create_dataset("y_train", data=np.array(y_train))
        h5file.create_dataset("x_validation", data=np.array(x_validation))
        h5file.create_dataset("y_validation", data=np.array(y_validation))
        h5file.create_dataset("x_test", data=np.array(x_test))
        h5file.create_dataset("y_test", data=np.array(y_test))
