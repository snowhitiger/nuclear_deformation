import numpy as np

def elliptic_flow(ecc2, k2=0.2, k2prime=0.1, delta2=0.1):
    '''match the spatial eccentricity ecc2 to elliptic flow v2'''
    v2_ = k2 * ecc2 + k2prime * ecc2**3.
    noise = np.random.uniform(-delta2, delta2, size=v2_.shape)
    return v2_ * (1.0 + noise)



def scale_entropy(s0, top_percent=0.02):
    '''get events of top-1% highest total entropy, use it
    to scale the s0 for all the events with that specific configuration.
    The reason is that the scale factor that matchs s0 to dN/dY is not known
    for different collision systems. This self-normalizing will remove the
    nuclear size and collision energy dependences when applied to real 
    experimental data'''

    top_ = int(len(s0)*top_percent)
    norm = np.mean(sorted(s0)[-top_:])
    print("norm factor = ", norm)
    return s0 / norm


if __name__ == '__main__':
    ecc2 = np.linspace(0, 1, 20)
    v2_ = elliptic_flow(ecc2)
    print(ecc2, v2_)

    s0 = np.linspace(10, 0, 2000)
    print("scaled_s0=", scale_entropy(s0))

